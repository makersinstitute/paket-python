import unittest
import time

from paket import paket

class TestPaketMethods(unittest.TestCase):

    def test_packed_message(self):
        print "test_packed_message"

        messsage = {
            'name': 'John',
            'token': 'ABCDE12345'
        }

        packed_message = paket.pack(messsage, 'pass', {});
        print packed_message

        unpacked_message = paket.unpack(packed_message, 'pass', {})
        print unpacked_message

        self.assertEqual(unpacked_message.get('name'), 'John')
        self.assertEqual(unpacked_message.get('token'), 'ABCDE12345')

    def test_packet_message_from_node(self):
        print "test_packet_message_from_node"

        # this paket valid for 100 years, expect error at that time
        packed_message = 'pkt15.1$eyJuYW1lIjoiSm9obiIsInRva2VuIjoiQUJDREUxMjM0NSJ9$4605220938$IjFCNjM2N0YzQTY2RTc4QTlCQ0QwRjk5NThDMEFEMDgxNDY1QjgwMDdBOUNBOTlCRjhDOUM1RTE4MjI1MUFEMzhDREUxRkI2RUM3MTlBOEYzMkQyQTEwQTYxODBBNDJGNzkyNjc4RjhEQjlFMTYyRDNDOEIxM0ZCRDBGMDU0MEMwIg=='

        unpacked_message = paket.unpack(packed_message, 'pass', {})
        print unpacked_message

        self.assertEqual(unpacked_message.get('name'), 'John')
        self.assertEqual(unpacked_message.get('token'), 'ABCDE12345')

    def test_packed_message_wrong_password(self):
        print "test_packed_message_wrong_password"

        messsage = {
            'name': 'John',
            'token': 'ABCDE12345'
        }

        packed_message = paket.pack(messsage, 'pass', {});
        print packed_message

        with self.assertRaises(Exception) as context:
            paket.unpack(packed_message, 'notpass', {})

        self.assertTrue('Invalid signature' in context.exception)

    def test_packed_message_time_expired(self):
        print "test_packed_message_time_expired"
        
        messsage = {
            'name': 'John',
            'token': 'ABCDE12345'
        }

        packed_message = paket.pack(messsage, 'pass', {});
        print packed_message

        time.sleep(35)

        with self.assertRaises(Exception) as context:
            paket.unpack(packed_message, 'pass', {})

        self.assertTrue('Expired' in context.exception)

if __name__ == '__main__':
    unittest.main()

