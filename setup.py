from setuptools import setup

setup(
    name='paket',
    version='0.1.0',
    description='Encrypt then HMAC',
    url='https://gitlab.com/makersinstitute/paket-python',
    author='Nurul Arif Setiawan',
    author_email='n.arif.setiawan@gmail.com',
    license='MIT',
    packages=['paket'],
    zip_safe=False
)
