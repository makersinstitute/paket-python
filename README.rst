
================================================
Currently only HMAC, encrypt will be added soon!
================================================

=======
Paket
=======

Encrypt then HMAC message content

Aim is to provide compatible implementation for some programming languages: 

* Nodejs `paket_node`_.
* Python (this module) `paket_python`_.
* Contribute more here

See `paket_md`_. for the standard

*********
Install
*********

.. code-block:: bash

    git clone https://gitlab.com/makersinstitute/paket-python
    pip install paket-python/

*******
Methods
*******

pack
====

.. code-block:: python

    packed = pack(message, password, options)

unpack
======
.. code-block:: python

    unpacked = unpack(message, password, options)

* :code:`message` dictionary containing data
* :code:`password` string password
* :code:`options` for packing or unpacking. currently not used


*******
License
*******

MIT

.. _paket_node: https://gitlab.com/makersinstitute/paket-node
.. _paket_python: https://gitlab.com/makersinstitute/paket-python
.. _paket_md: https://gitlab.com/makersinstitute/paket-node/blob/master/PAKET.md
