import hmac, hashlib, json, time, base64

prefix = 'pkt15.1'
separator = '$'
expire_in_seconds = 300

def sign_string(data, password):
    return hmac.new(password, data, hashlib.sha512).hexdigest().upper()

def verify_signature(data, signature, password):
    return ct_compare(hmac.new(password, data, hashlib.sha512).hexdigest().upper(), signature)

def verify_time(expiration):
    return (int(time.time()) - int(expiration)) <= 0

def ct_compare(a, b):
    """
    ** From Django source **
    Run a constant time comparison against two strings
    Returns true if a and b are equal.
    a and b must both be the same length, or False is 
    returned immediately
    """
    if len(a) != len(b):
        return False

    result = 0
    for ch_a, ch_b in zip(a, b):
        result |= ord(ch_a) ^ ord(ch_b)
    return result == 0

def generate_mac(message, password):
    expiration = int(time.time()) + expire_in_seconds
    encoded_message = base64.urlsafe_b64encode(json.dumps(message).replace(" ", ""))
    payload = prefix + separator + encoded_message + separator + str(expiration)
    signature = sign_string(payload, password)
    encoded_signature = base64.urlsafe_b64encode(signature)
    return payload + separator + encoded_signature

def pack(message, password, options):
    return generate_mac(message, password);

def unpack(message, password, options):
    parts = message.split(separator)
    if len(parts) != 4:
        raise Exception('Incorrect paket parts') 

    mac_prefix = parts[0]
    encoded_message = parts[1]
    expiration = parts[2]
    encoded_signature = parts[3]
    payload = prefix + separator + encoded_message + separator + expiration

    # seems like urlsafe_b64decode add extra double qoutes ", so we need to replace it
    decoded_signature = base64.urlsafe_b64decode(encoded_signature).replace('"','')
    
    if not(verify_signature(payload, decoded_signature, password)):
        raise Exception('Invalid signature') 

    if not(verify_time(expiration)):
        raise Exception('Expired') 

    decoded_message = base64.urlsafe_b64decode(encoded_message)

    return json.loads(decoded_message)


